CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The One Time Link module creates short and secure links for one-time login. Each
link has a configurable expiration date. Each link can be used only once.

 * For a full description of the module visit:
   https://www.drupal.org/project/one_time_link

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/one_time_link


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the One Time Link module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to admin/modules and enable the module.
    2. Navigate to admin/config/generate-one-time-link and enter the User Name
       or Email.
    3. A one time login link will be generated.


MAINTAINERS
-----------

 * sujit Kumar (sujit.k) - https://www.drupal.org/u/sujitk
